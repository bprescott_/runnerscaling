#!/bin/sh

if [ "x$1" = "x" -o ! -r $1 ] ; then
  echo "usage: $0 <runnerlog>" 1>&2
  echo "no file passed as parameter or file not readable" 1>&2
  exit 1
fi

jbdir="./lifecycle_job"
[ -d $jbdir ] && rm -f $jbdir/* && rmdir $jbdir

lcdir="./lifecycle_runner"
[ -d $lcdir ] && rm -f $lcdir/* && rmdir $lcdir

ivdir="./inventory_runner"
[ -d $ivdir ] && rm -f $ivdir/* && rmdir $ivdir

jidir="./inventory_jobs"
[ -d $jidir ] && rm -f $jidir/* && rmdir $jidir

mkdir $lcdir
mkdir $ivdir
mkdir $jbdir
mkdir $jidir

runnerct=0

cat $1 | while read loge ; do
  unset runnerc1 runnerc1f runnerc1v
  unset runnerc2 runnerc2f runnerc2v runnern
  unset jobn jobf jobb repo
  runnerc1=$(echo "${loge}" | sed 's~^.*\(name\)=\([^ ]*\) .*~\1 \2~g')
  set -- $runnerc1
  runnerc1f=$1
  runnerc1v=$2
  runnerc2=$(echo "${loge}" | sed 's~^.*\(name\)=\([^ ]*\)$~\1 \2~g')
  set -- $runnerc2
  runnerc2f=$1
  runnerc2v=$2


  if echo "${loge}" | grep -q 'job=' ; then
    jobn=$(echo "${loge}" | sed 's~^.*\(job\)=\([^ ]*\) .*~\2~g')
  else
    :
  fi


  if [ "x$runnerc1f" = "xname" ]; then
    runnern=$runnerc1v
  elif [ "x$runnerc2f" = "xname" ]; then
    runnern=$runnerc2v
  fi


  if echo "${loge}" | egrep -q 'journal: Suppressed .* messages'
  then
    echo "${dst} R:$runnerct J:$jcnt         =================================== missing messages"
  elif echo "${loge}" | grep -q 'Job succeeded'
  then
    # job finishes
    jobf='yes'
  elif echo "${loge}" | grep -q 'Problem while reading command output'
  then
    :
  elif echo "${loge}" | grep -q 'Checking for jobs... received'
  then
    # job begins
    jobb='yes'
    repo=$(echo "${loge}" | sed 's~^.*\(repo_url\)=\([^ ]*\) .*~\2~g')
  elif echo "${loge}" | grep -q 'Checking for jobs... failed'
  then
    :
  elif echo "${loge}" | grep -q 'job failed'
  then
    # job finishes
    jobf='yes'
  elif echo "${loge}" | grep -q 'Job failed:'
  then
    # job finishes
    jobf='yes'
  elif echo "${loge}" | grep -q 'Finished docker-machine build: '
  then
    jobf='maybe'
  elif echo "${loge}" | grep -q 'ERROR: fatal error: runtime: out of memory'
  then
    echo "${dst} R:$runnerct J:$jcnt         =================================== out of memory"
    runnerignore="yes"
    :
  elif echo "${loge}" | grep -q 'Failed to process runner'
  then
    :
  elif echo "${loge}" | egrep -q 'Logs begin at|Detecting the provisioner...'
  then
    :
  elif echo "${loge}" | egrep -q 'Appending trace to coordinator|Detecting operating system of created instance|'
  then
    :
  elif echo "${loge}" | egrep -q 'Starting docker-machine build...|Tags are not key value in pairs|Waiting for machine to be running'
  then
    :
  elif echo "${loge}" | egrep -q 'Using existing docker-machine|Docker is up and running|To see how to connect your Docker Client to'
  then
    :
  elif echo "${loge}" | egrep -q 'Running pre-create checks...|Launching instance...|Provisioning with.*operation=create|Waiting for SSH to be available...'
  then
    :
  elif echo "${loge}" | egrep -q 'Machine .* was stopped.*operation=stop|WARNING: Removing machine|Created docker-machine|Creating machine...'
  then
    :
  elif echo "${loge}" | egrep -q 'About to remove runner.*operation=remove|WARNING: This action will delete both local reference'
  then
    :
  elif echo "${loge}" | egrep -q 'Stopping.*operation=stop|Successfully removed.*operation=remove|Machine removed'
  then
    :
  elif echo "${loge}" | egrep -q 'Cleaned up docker-machine|Submitting job to coordinator...|Copying certs to the remote machine...'
  then
    :
  elif echo "${loge}" | grep -q 'Error while executing file based variables removal script|Setting Docker configuration on the remote'
  then
    :
  elif echo "${loge}" | egrep -q 'WARNING: Error while stopping machine|WARNING: Requesting machine removal'
  then
    :
  elif echo "${loge}" | egrep -q 'WARNING: Stopping machine|WARNING: Machine creation failed, trying to provision'
  then
    :
  elif echo "${loge}" | egrep -q 'ERROR: Failed to remove network for build|ERROR: Failed to cleanup volumes'
  then
    :
  elif echo "${loge}" | egrep -q 'ERROR: runtime|ERROR.*local/go/src/runtime|WARNING: Skipping machine removal, because it'
  then
    # something has gone wrong, the runner in this log entry isn't real
    runnerignore="yes"
  else
    echo "UNKNOWN ${loge}"
  fi

  dst=$(echo "${loge}" | awk '{ print $1" "$2" "$3 }')

    # baseline job count

  jcnt=$(ls ${jidir}/* 2>/dev/null | wc -l)

  if [ "x$runnern" = "x" ]; then
    :
  else
    echo "${loge}" >> ${lcdir}/"${runnern}"

    if echo "${loge}" | grep -q 'Machine created '; then
      if [ -f ${ivdir}/"${runnern}" ]; then
        :
      else
        touch ${ivdir}/"${runnern}"
        runnerct=$(ls ${ivdir} | grep -c runner)
        echo "${dst} R:$runnerct J:$jcnt                                       ($runnern added)"
      fi
    elif echo "${loge}" | grep -q 'Machine removed '; then
      if [ -f ${ivdir}/"${runnern}" ]; then
        didjobs=$(grep -vc grep ${ivdir}/"${runnern}")
        rm -f ${ivdir}/"${runnern}"
        runnerct=$(ls ${ivdir} | grep -c runner)
        echo "${dst} R:$runnerct J:$jcnt                                       ($runnern removed, did $didjobs jobs)"
      else
        :
      fi
    elif [ "x${runnerignore}" = "xyes" ]; then
      :
    else
      if [ -f ${ivdir}/"${runnern}" ]; then
        :
      else
        touch ${ivdir}/"${runnern}"
        runnerct=$(ls ${ivdir} | grep -c runner)
        echo "${dst} R:$runnerct J:$jcnt                                       ($runnern inherited)"
      fi
    fi
  fi

  # jobs: log their activity to per-job logs
  # tag them to their runner

  if [ "x$jobn" = "x" ]; then
    :
  else
      # log job arrival once
    if [ -f ${jbdir}/"${jobn}" ] ; then
      :
    elif [ "x$jobb" = "xyes" ]; then
      echo "${dst} R:$runnerct J:$jcnt                  New job: ${jobn} (${repo})"
    else
      :
    fi
    echo "${loge}" >> ${jbdir}/"${jobn}"
    if [ "x$runnern" = "x" ]; then
      :
    elif [ -f ${ivdir}/"${runnern}" ]; then
      if egrep -q "${jobn}x" ${ivdir}/"${runnern}" ; then
        : # already noted
      else
        echo "${jobn}x" >> ${ivdir}/"${runnern}"
      fi
    fi

  # jobs: maintain inventory of what's running

    if [ "x$jobf" = "xyes" ]; then
      if [ -f "${jidir}/${jobn}" ]; then
        rm -f "${jidir}/${jobn}"
          # change: how many jobs?
        jcnt=$(ls ${jidir}/* 2>/dev/null | wc -l)
        echo "${dst} R:$runnerct J:$jcnt                  Job ${jobn} ended"
      else
        :
      fi
    elif [ "x$jobf" = "xmaybe" ]; then
      # snag additional log entries which recreate job in inventory
      :
    elif echo "${loge}" | grep -q 'WARNING:' ; then
      # sometimes the job ends, and then there's warnings for it. ignore them
      :
    elif [ ! -f "${jidir}/${jobn}" ]; then
      touch "${jidir}/${jobn}"
          # change: how many jobs?
      jcnt=$(ls ${jidir}/* 2>/dev/null | wc -l)
    else
      :
    fi
  fi
done

ls ${ivdir} | while read runnern ; do
  didjobs=$(grep -vc grep ${ivdir}/"${runnern}")
  jcnt=$(ls ${jidir}/* 2>/dev/null | wc -l)
  runnerct=$(ls ${ivdir} | grep -c runner)
  echo "END:            R:$runnerct J:$jcnt $runnern left, having run $didjobs jobs"
done
