# purpose

summarises a auto-scaling runner log
shows

- jobs arriving
- machines being started and stopped, how many jobs they ran
- how many machines are running (based on churn in log)

# useful output

- STDOUT has analysis of what's running and changes as they occur
- Each processed log entry is written to a file in either or both of: `lifecycle_job/`, `lifecycle_runner/`
  - activity for a given job or runner can be found in the these files once the analysis completes
- `inventory_jobs/`, `inventory_runner/` keep track of what runners and jobs are live

# example STDOUT

`R:7 J:1` indicates that 7 runners are running, and one job

```text
Jan 20 08:36:29 R:7 J:1                  New job: 469086
Jan 20 08:36:29 R:7 J:2                  New job: 469089
Jan 20 08:36:33 R:6 J:3 (runner-19febdcc removed, did 1 jobs)
Jan 20 08:37:11 R:6 J:3                  New job: 469091
Jan 20 08:37:35 R:6 J:3                  Job 469086 ended
Jan 20 08:38:32 R:7 J:3 (runner-2d28fdff added)
Jan 20 08:38:57 R:7 J:2                  Job 469091 ended
Jan 20 08:43:36 R:6 J:2 (runner-4936dc0f removed, did 2 jobs)
Jan 20 08:44:35 R:5 J:2 (runner-2d28fdff removed, did 0 jobs)
END:            R:5 J:2 runner-8e128fb5 left, having run 6 jobs
END:            R:5 J:2 runner-1dab05ed left, having run 5 jobs
END:            R:5 J:2 runner-a24a757d left, having run 4 jobs
END:            R:5 J:2 runner-615c61dd left, having run 3 jobs
END:            R:5 J:2 runner-f4330178 left, having run 2 jobs
```

# usage

- clone repo
- execute: `./logprocessor.sh /path/to/runnerlog`
